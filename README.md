Mediacloud-Graph
================

Description
-----------
A couple scripts to graph MediaCloud output.
There's a simple Ruby script to parse the gexf MediaCloud output, which itself outputs JSON.
The JSON is read into a js script that uses d3 to generate a force-directed graph.

There will be a slider to allow one to see how the graph changes through time.  That's really the purpose of this.

License
-------
GPLv3

Copyright
---------
2012 President and Fellows of Harvard College